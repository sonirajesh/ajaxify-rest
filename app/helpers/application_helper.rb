module ApplicationHelper
  def sortable(column, title = nil)
    title ||= column.titleize
    css_class = column == sort_column ? "current #{sort_direction}" : nil
    direction = column == sort_column && sort_direction == "asc" ? "desc" : "asc"
    link_to title, params.merge(:sort => column, :direction => direction, :page => nil,:per_page => params[:per_page]), {:class => css_class}
  end

  def perpage(e)
	css_class = e == params[:per_page] ? "current" : nil
	link_to e, params.merge(:per_page => e, :page => nil), {:class => css_class}    
  end
end
