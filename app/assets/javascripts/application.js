// This is a manifest file that'll be compiled into application.js, which will include all the files
// listed below.
//
// Any JavaScript/Coffee file within this directory, lib/assets/javascripts, vendor/assets/javascripts,
// or vendor/assets/javascripts of plugins, if any, can be referenced here using a relative path.
//
// It's not advisable to add code directly here, but if you do, it'll appear at the bottom of the
// compiled file.
//
// Read Sprockets README (https://github.com/sstephenson/sprockets#sprockets-directives) for details
// about supported directives.
//
//= require jquery
//= require jquery_ujs
//= require turbolinks
//= require_tree .

$(function() {
  $(document.body).on("change", "#per_page",  function() {
    submit_search();
  });

  $(document.body).on("click", "#products th a, #products .pagination a, .per_page a, .delete",  function() {
  	start_loader();
  	console.log(this.href);
    $.getScript(this.href);
    return false;
  });

  $(document.body).on("click", ".delete",  function(e) {
  	e.preventDefault();
  	start_loader();
  	var dataid = $(this).attr("dataid");
    $.ajax({
    	url: this.href,
    	method: 'delete',
    	data: { id: dataid }
    });
    return false;
  });

  $("#products_search input").keyup(function() {
    submit_search();
  });
});

function start_loader(){
	$('#products').html('<div class="loader"><img src="http://www.intrahealth.org/images/loading.gif"></div>');
}

function submit_search(){
	start_loader();
	console.log($("#products_search").serialize());
	$.get($("#products_search").attr("action"), $("#products_search").serialize(), null, "script");
	return false;
}