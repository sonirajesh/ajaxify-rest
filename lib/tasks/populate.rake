namespace :db do
  desc "Erase and fill database"
  task :populate => :environment do
    require 'populator'
    require 'faker'
    
    [Product].each(&:delete_all)
      Product.populate 50..100 do |product|
        product.name = Populator.words(1..5).titleize
        # product.description = Populator.sentences(2..10)
        product.price = [4.99, 19.95, 100]
        product.released_at = 2.years.ago..Time.now
        product.created_at = 2.years.ago..Time.now
      end
    
    # Person.populate 100 do |person|
    #   person.name    = Faker::Name.name
    #   person.company = Faker::Company.name
    #   person.email   = Faker::Internet.email
    #   person.phone   = Faker::PhoneNumber.phone_number
    #   person.street  = Faker::Address.street_address
    #   person.city    = Faker::Address.city
    #   person.state   = Faker::Address.us_state_abbr
    #   person.zip     = Faker::Address.zip_code
    # end
  end
end